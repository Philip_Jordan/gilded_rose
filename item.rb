
class Item
  attr_accessor :name, :sell_in, :quality

  def initialize(name, sell_in, quality)
    @name = name
    @sell_in = sell_in
    @quality = quality
  end

  def to_s()
    "#{@name.ljust(50)} \t\t\t\t#{@sell_in.to_s.ljust(5)} \t\t\t\t#{@quality}"
  end
end